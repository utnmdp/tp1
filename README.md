# Laboratorio V

##Trabajo practico N°1: Hilos

###¿Cuantos consumidores se necesitan para que el stock llegue a cero?
Se necesita al menos un consumidor que consuma constantemente lo que produce el productor.
Cuantos mas consumidores, mas rapido se terminara el stock de BeerHouse, ya que el productor tarda un tiempo
en producir pero los consumidores consumen constantemente

###¿Por que usar bloques synchronized?
synchronized es un modificador que viene incluido en Java, 
nos permite sincronizar metodos para prevenir inconsistencias 
cuando un objeto es accesible desde distintos hilos.

Todos los métodos synchronized de la misma instancia son mutuamente excluyentes entre sí. 
Es decir que, dado un objeto compartido por más de un thread, 
sólo uno de ellos puede acceder en un determinado momento a uno de esos métodos de instancia.

Por ejemplo si un hilo incrementa el contador, 
mientras otro lo decrementa, y otro lo lee, 
puede que el resultado no sea el esperado si hay concurrencia.
Para eso las lecturas y escrituras de un objeto compartido, se hacen a traves de metodos sincronizados.

###Explicar que es un recurso compartido
El recurso compartido es la instancia a la cual todos los hilos trataran de acceder
para realizar una accion, en este caso BeerHouse seria el recurso compartido, ya que
el productor tratará de usarlo para agregar cervezas al stock, y los consumidores lo usaran
para sacar cervezas del stock al consumirlas.

###Explicar las tres formas de instanciar un Thread
La primera forma de instanciar un thread es  declarando una subclase de la clase Thread,
es decir, crear una clase X que extienda de la clasa Thread. De esta forma la
clase hija tiene acceso a todos los metodos de la clase Thread como por ejemplo el metodo run para empezar a usar el hilo.

Otra forma de instanciar un thread es creando una clase que implemente la interface Runnable, al hacer esto la clase
tendra que definir redefinir el metodo run y start de la interface (La clase Thread implementa la interface, Runnable)

La tercera forma es haciendo el new de Thread y pasandole la clase por parametro

Se suele utilizar la primera forma, cuando la clase declarada no tenga que ser
subclase de ninguna otra superclase. 