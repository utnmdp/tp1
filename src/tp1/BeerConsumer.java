package tp1;

public class BeerConsumer extends Thread {

    private int id;
    private BeerHouse beerHouse;

    public BeerConsumer(int id, BeerHouse beerHouse) {
        this.id = id;
        this.beerHouse = beerHouse;
    }

    @Override
    public void run() {
        // El consumidor intenta consumir constantemente cervezas
        while(true) {
            // Le pide una cerveza a BeerHouse
            String beer = this.beerHouse.get();

            System.out.printf("BeerConsumer: El consumidor [%s] consumió la cerveza [%s] \n", this.id, beer);
        }
    }
}
