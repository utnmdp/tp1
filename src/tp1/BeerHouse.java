package tp1;

import java.util.ArrayList;

public class BeerHouse {

    private ArrayList<String> stock;
    private int maxStock;

    public BeerHouse(int maxStock) {
        this.stock = new ArrayList<String>();
        this.maxStock = maxStock;
    }

    /**
     * Cuando el productor produce una nueva cerveza lo agrega al stock
     * @param beer - La cerveza que producio
     */
    public synchronized void put (String beer) {
        // Mientras que el stock este al maximo no lo deja agregar mas
        while(this.stock.size() >= this.maxStock) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // Si el stock no esta al maximo se agrega al stock
        this.stock.add(beer);

        System.out.printf("BeerHouse: [put] El stock quedó en %s \n", this.stock.size());

        notify();
    }


    /**
     * Permite que un consumidor consuma una cerveza del stock
     * @return String - Nombre de la cerveza que se consumio
     */
    public synchronized String get() {
        // Mientras que no haya stock disponible no deja que nadie consuma cerveza
        while(this.stock.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // Siempre se le da la ultima cerveza que entro al stock (porque soy vago)
        String beer = this.stock.get(this.stock.size() - 1);

        // Saca la cerveza que se consumio
        this.stock.remove(beer);

        notify();

        System.out.printf("BeerHouse: [get] El stock quedó en %s \n", this.stock.size());

        // Retorna la cerveza al consumidor
        return beer;
    }
}
