package tp1;

public class BeerProducer extends Thread {

    private int id;
    private BeerHouse beerHouse;

    public BeerProducer(int id, BeerHouse beerHouse) {
        this.id = id;
        this.beerHouse = beerHouse;
    }

    @Override
    public void run() {
        // El productor produce constantemente cervezas
        while(true) {
            // Genera el nombre de la cerveza a producir
            String beer = "cerveza_" + (int)(Math.random() * 100000) + 1;

            System.out.printf("BeerProducer: El productor [%s] está produciendo la cerveza [%s] \n", this.id, beer);

            // Agrega la cerveza producida al stock de BeerHouse
            this.beerHouse.put(beer);

            System.out.printf("BeerProducer: El productor [%s] produció la cerveza [%s] y se la dio a BeerHouse \n", this.id, beer);

            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
