package tp1;

public class Main {

    private static Thread[] consumers;
    private static BeerHouse beerHouse;
    private static BeerProducer beerProducer;

    public static void main(String[] args) {
        BeerHouse beerHouse = new BeerHouse(100);
        BeerProducer beerProducer = new BeerProducer(1, beerHouse);
        Thread[] consumers = new Thread[5];

        System.out.print("Main: El productor [1] empieza a producir \n");
        beerProducer.start();

        for(int i = 0; i < 5; i++) {
            consumers[i] = new BeerConsumer(i, beerHouse);

            System.out.printf("Main: El consumidor [%s] empieza a consumir \n", i);
            consumers[i].start();
        }
    }
}